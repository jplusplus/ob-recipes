Title: The basics
Summary: How we count wealth for individuals and for companies.
Date: 2015-02-15 08:50:28
Author: nkb
Category: Basics
Slug: basics
Lang: en

## Counting wealth

For individuals, counting money is easy. You own what you own. You can own different types of things, such as the money on your bank account, the cash in your wallet and maybe a car or, if you’re lucky, a house.

![Image](../basics/image04.png)

Of course, individuals can owe money, too. In this case, you have debt, such as a mortgage for your house.

![Image](../basics/image06.png)

For companies or any other legal entity, things can be seen as a little different. What a company owns is called **assets**. What it owes is called **liabilities**. The difference between the two is called **equity**. “Equity” represents the value of the company for its owners. Given a fixed asset value, a higher debt load means the equity has less value (the company isn’t worth much). If there is no debt, the equity is equal to all the assets. A basic rule in accounting is that, by definition, assets must always be perfectly equal to the sum of liabilities and equity.

Because assets always balance the liabilities, this is called a **balance sheet**.

![Image](../basics/image11.png)


## Money flows

Balance sheets tell us what a company or an NGO owns. To know how it operates, we must look at the **income statement**. The income statement registers what the company earns and what it pays out for a given period of time, usually one year. Whatever the company sells is counted as income. Whatever it pays and disburses money for is an expenditure.

Loans do not appear in the income statement. If they did, a company would just have to take a lot of loans to appear profitable! Loans can only be found in the balance sheet.

The statement of income alone is not enough to understand if a company is doing well or not. A company can make a small profit, but if the balance sheet shows that it owes an enormous amount of debt, it means the company is not in good shape. Conversely, a company can have a bad year and make a loss on the income statement, but the balance sheet can show that the company has lots of cash in the bank and can keep going.

![Image](../basics/image10.png)
_Financial Statement 1: A successful wood factory_

![Image](../basics/image08.png)
_Financial Statement 2: A start-up company with few employees that is just starting to generate profit_

![Image](../basics/image07.png)
_Financial Statement 3: A steel mill that has a rough year_

Accounting does it best to help understand the financial state of an entity when it combines statements of income, balance sheets and cash flow statements (a reconciliation of the movement in the cash position of such entity from the beginning to the end of the period considered - typically a year).


> It’s important to remember that both documents, statement of income and balance sheets, show totally different things but are equally important. The statement of income is for **flows**, i.e amounts over a period of time, usually one year. The balance sheet is for **stock**, i.e an amount at a given date (usually December 31st).


