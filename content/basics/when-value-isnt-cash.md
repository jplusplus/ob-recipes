Title: When Value isn’t Cash
Summary: Advanced tricks to count wealth when cash and value become two different things.
Date: 2015-02-11 08:50:28
Author: nkb
Category: Basics
Slug: accrual-accounting
Lang: en

## Cash-based vs. accrual accounting

For us normal people, we consider our wealth and revenues to be the actual cash we have in our pocket or in our bank account. If we have debts or assets (such as a house or a car), we don’t add it to our bank statement - we just know we have it.

It’s different for organizations. They mostly use what’s called accrual accounting, which is a way to keep track of assets and liabilities as well as cash income and spending. Accrual accounting is great at keeping track of value and not cash. If a city buys a building, its cash reserves are decreased but a new asset of equivalent value is created. In the end, the total of the balance sheet remains the same.

Accrual accounting let accountants and controllers have a better vision of the financial health of an organization. Using cash only, it’s hard to keep track of liabilities. An organization can take several loans and have plenty of cash - even though the amount it’ll have to pay back put it in a terrible situation.
Accrual accounting enables things like amortization and provisions, explained below.

> **Germans beware!**
> 
> Almost everyone uses accrual accounting, including public administrations. However, most local administrations in Germany and Austria still use cash-based accounting. Before you dive in budget data, check if it’s cash-based or accrual. If a budget has terms like assets, liabilities, amortization or provision, then it’s accrual.


## Amortization

When a building is built by a local administration (or a company), it writes it as an asset , but not in the statement of income. If it did write it as an expense, the statement of income would look very bad one year and much better the next. It would be impossible to use it to understand how the organization works. Instead, investments such as a building are amortized over several years.
Every year, the value of the building in the balance sheet is reduced by a 40th of its initial value, so that after 40 years, the book value of the building is nil. Every year, this reduction in value also appears in the statement of income. The local administration incurs an expense of the same amount. The administration loses the amount in its books, even if it does not disburse (pays) anything. It’s very important to realize that disbursements (actual money transfers) are just one part of accounting.

The duration of an amortization is fixed by law. Buildings are usually amortized over 40 years, cars over 10 years, computers over 3 years etc. Beware that the law is not the same in every country. There are many more rules (e.g when a building is renovated) but the basic concept remains the same: the book value of all assets decrease with time and the rate of decrease is set by law.

> **Why do we need amortization?**
> 
> What if the actual value of the building actually increases with time? If a company built an office building in London 40 years ago, surely the value is not 0 today! In such cases, the book value of the company would not be the same as its actual value. Some countries allow for companies to assess the value of their assets at their “true” value. But how can we make sure that companies correctly assess the value of their assets? It’s easy enough to know the value of a building in central London, but what if a company builds a stadium? Is there a “true” market value for this?
> 
> Not using amortization rules gives so much freedom to companies that their books reflect nothing more than what they think they are worth. Needless to say, many use this opportunity to appear richer than they actually are. The Enron scandal, for instance, is in large part due to the company’s freely (and wrongly) assessing the value of its assets.


## Provisions

When you know you’ll have to pay something in the future, you can make **provisions**. These are expenses that haven’t been disbursed yet but that will be disbursed in the future. In the statement of income, they look exactly like disbursed items. If an administration is responsible for paying the pensions of its employees, for instance, it must add provisions to its statement of income to reflect this fact and help plan ahead.

