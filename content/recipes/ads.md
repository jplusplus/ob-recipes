Title: Ads, ads, ads
Summary: For a yummy political career, you need juicy publicity. It tastes even better if you're not the one paying for it!
Tags: highlight
Slug: ads
Lang: en
Ingredients: 
	A ministerial or similar high ranking public position
	Helping hands in the executive board of a state-owned company
	A family member or equivalent who owns a major tabloid 
Serves: €500,000 to €4 million
Risk: Low
Duration: Unlimited


**1. Start your political career** with the help of your yellow press friends. Rely on the support of well-placed articles and perfectly timed campaigns to make your way to the top. 

**2. Once you accessed a position of power** within a public administration, stay close to your helping hands in the yellow press. Always make sure to coordinate and agree when planning to push for a law or inventing a new political strategy as a Minister, Chancellor or Commissioner. With such backing, you largely avoid annoying discussions within your own party. Such shenanigans would give your dish a slightly bitter taste. So, never forget to keep in touch with your friends in the press!

**3. Interlink your yellow press family** with your friends in state-owned company! Get state-owned companies to place ads in your friends' tabloids. They don't need to be anything else than PR in favor of your ministry or yourself as a politician. Publishers and editors will love you for the fresh money and you will get more publicity than you could ever have had by only using just the regular, political advertising budgets.

**4. Attention! To avoid legal consequences** make sure that the state-owned companies that did the advertising cannot claim they were defrauded (because they paid for your personal PR instead of placing ads that promoted their business). You know, people always eat with their eyes... So the output must pass as "positive information- and PR-character" for the company. The executives of the state-owned companies must confirm that the ads supported the communication targets of the companies and helped improve their public image. Then you can relax: you will not be convicted. 

As you experienced cooking this meal took quite a long time so if you have come so far make sure to really enjoy every ad, uhm, every bite! 

!!! inreallife "In real life"
	In **Austria**, Werner Faymann (later an Austrian Chancellor and at that time -2007 - transport minister) and Josef Osermayer (later secretary of State for Media) were suspected of a breach of trust when it came to public that ASFINAG (a state-owned highway operator) and ÖBB (the state-owned train company) placed expensive ads in _Kronenzeitung_ repeatedly over years containing nothing but PR for the transport ministry and Faymann himself. 

	The investigation came to a stop in 2013 because, according to the public prosecutors "no damage for the companies" had been caused. The investigation found out that the ads had been supportive to the communication targets of that time (2007/2008) and had a "positive information and advertising character", which successfully improved the public images of the companies. Werner Faymann and media mogul Hans Dichand (which Faymann called his "uncle") were very close until Faymann broke the relationship in 2016.

	Read more at Profil.at: [Inseratenaffäre: Geheime Dokumente belasten Faymann schwer](http://www.profil.at/home/inseratenaffaere-geheime-dokumente-faymann-338843).

	![An advertorial paid for by the OBB.](../recipes/faymann_inseratenaffaere.jpg)

	<small>One of 24 double-page advertorials paid for by the OBB in _Kronenzeitung_. Faymann had his own column in the newspaper.</small>

	In **Latvia**, Riga mayor Nils Ushakov used city funds to pay for his sponsored posts on Facebook and for the acquisition of followers on social media. Read more at [TVnet.lv](http://www.tvnet.lv/zinas/latvija/565759-knab_usakovs_drikst_reklameties_pasvaldibas_apmaksataja_reklama_par_veloinfrastrukturas_attistibu).

!!! whatolookfor "What to look for"
	If a state-owned company advertises suspiciously in the same media group over and over again, start looking for links between the heads of the company and those of the media group.