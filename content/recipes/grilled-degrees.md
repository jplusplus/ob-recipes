Title: Grilled Degrees
Summary: Have a university? The sale of degrees could provide you with some sweet, sweet income.
Slug: grilled-degrees
Lang: en
Ingredients: 
	A university
Serves: €3,000 per degree
Risk: Medium
Duration: 5 years or more

**1. Become head of a university.** Top jobs at universities go to academics, though these have to be academics with a serious political pedigree (membership in the local dominating party is often required).

**2. Set up a recruiting bureau for oversees students.** Oversees students (in the European Union: students from outside the EU) typically pay much higher fees than locals, usually four times as much. This money goes directly to the university's coffers, which are more often than not empty as funding from the central government is cut.

**3. Place trusted persons as recruiting agents.** Make sure you have direct links with your recruiting agents abroad. Have a trusted person hire them in the country of recruitement (usually China).

**4. Start selling!** Universities usually require a high level of fluency in English or in the local language, as well as other prerequisites in the field of study. Tell your recruiting agents that these barriers to entry can be lowered for a fee, usually €3,000. With some skills, such a system can be done with minimal publicity. You can, for instance, personally vet the students who paid the fee so that university staff never see their files before students are officially enrolled. Your colleagues might have suspicions, but they'll never have proof. Because oversees students bring in much needed cash to the university, they won't talk.

**5. (Optional) Sell grades.** Once on campus, your students might have a hard time studying if they don't speak the language. You can offer to increase their grades or give them their degree for another fee. This will be more visible than the application fee and makes this recipe much more dangerous!

!!! inreallife "In real life"
In *France*, the head of the University of Toulon was sentenced to two years in jail for having organized a system of corruption whereby foreign students could bypass the admission system for a fee of 3,000€ ([read more at France3](http://france3-regions.francetvinfo.fr/provence-alpes/bouches-du-rhone/metropole-aix-marseille/marseille/faux-diplomes-ex-president-universite-toulon-veut-plaider-relaxe-907341.html) [fr]).

In *Australia*, an investigation by the Australian Broadcasting Corporation showed how universities organized corrupt practices to attract foreign students (watch [Degrees of Deception](http://www.abc.net.au/4corners/stories/2015/04/20/4217741.htm) on ABC).

In the *United Kingdom*, where higher education was deregulated first, corrupt practices involving foreign students were commonplace as soon as 2004 ([read more in The Guardian](https://www.theguardian.com/uk/2004/aug/08/highereducation.education)).

!!! whatolookfor "What to look for"
Researching corruption at university is extremely hard, because universities live off their reputation. If a corruption story mars a member of the university, the whole staff, students and alumni suffers. The incentives to preserve the rule of silence are high. Contact to former students or staff is your best chance.

