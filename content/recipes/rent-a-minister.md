Title: Rent-a-minister
Summary: Enjoy a good meal and get paid for it!
Lang: en
Slug: 
Ingredients: 
	A position in the government
Serves: up to 1 million
Risk: None
Duration: unlimited

**1. Reach the top of the administration.**
For this recipe to work, you must be at the very top of the administrative ladder _and_ be a celebrity. Ministerial or presidential positions work, but are not enough without a star status, which comes from a sustained relationship with your country's mass media.

**2. Have someone sell access to private meetings**. You must not let it be known that people pay to meet you. You need plausible deniability that you were ignorant of the practice. Not because it's illegal, but because it'll make you look cheap. A third-party, preferably a non-profit organization, should organize the meetings and charge customers.

**3. Enjoy!** You just have to talk to people over lunch or in regular meetings and the cash will flow. You don't need to reveal anything to your customers, just to make them feel special like you do with voters.

!!! inreallife "In real life"
In **Germany**, you could pay up to 10,000€ for a meeting with high-ranking politicians from the social-democratic party ([read more in Der Spiegel](http://www.spiegel.de/politik/deutschland/spd-beendet-rent-a-sozi-gespraechsreihe-a-1122702.html)). 

In the **United States**, a charity sells access to president-elect Donald Trump for millions of dollars. Services range from a single photo opportunity to a multi-day fishing trip ([read more at the Center for Public Integrity](https://www.publicintegrity.org/2016/12/19/20564/donald-trumps-sons-behind-nonprofit-selling-access-president-elect)). 

!!! whatolookfor "What to look for"
Because payments go through third-parties, they are hard to trace in public budget data. A starting point might be the agendas of the politicians or administrators suspected of selling meetings. However, such meetings could be absent from official calendars because they would qualify as private encounters.