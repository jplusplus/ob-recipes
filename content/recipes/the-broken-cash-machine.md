Title: The Broken Cash Machine
Summary: The classic appetizer of using cash and goods paid with public money for your personal use
Lang: en
Slug: the-broken-cash-machine
Ingredients: 
	Working in a state-owned company that sells goods and services (think of ferry lines that sell snacks on board for instance)
Serves: €50 to €20,000 / month
Risk: Low
Duration: 20 years

**1. Get a job within a state-owned company that sell good or services to individuals.**
Access a position where you can control the cash machine. If you are a part of the trade union it's even better.

**2. Use the help of your colleagues to ensure that the customers are paying mostly in cash**, pretending the cash machine is broken is a good way. When they pay in cash, never print a receipt to not spoil your recipe. At the end of the day, you will have a lot of banknotes in your pocket for your personnal use. 

If the appetizer has not been enough, you can follow the next step to transform this recipe in a complete dish.

**3. Help yourself in the storage room of the company.** You can delight your friends with the item you took (don't sell the products to strangers on Internet, it's a bad idea).

**4. Enjoy! And don't forget to share with your colleagues.** It's the guarantee of less risks and more cash.


!!! inreallife "In real life"
In 2005, auditors found that the employees of SNCM, a French ferry company, [were sharing for personal use an important part of on-board sales](http://www.leparisien.fr/economie/scandale-a-la-sncm-17-10-2005-2006389771.php) (tobacco, alcohol, food, derivatives ...) performed during the crossing between the mainland and Corsica. The board of SNCM confessed that it was a part of their "social policy".

!!! whatolookfor "What to look for"
In many countries, the **income statement** of a state-owned company is a public documents by law. If the company you're investigating has zero sales of goods when the competitors have billions, you know that something is wrong.
