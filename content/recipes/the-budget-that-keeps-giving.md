Title: The Budget that Keeps Giving
Summary: Budgetary debates can be tough. Skip them and go straight for the budget changes!
Slug: the-budget-that-keeps-giving
Lang: en
Ingredients: 
	An executive position in any administration
Serves: n/a
Risk: None
Duration: Unlimited


**1. Make a preliminary budget** and keep it low. Make sure your friends at the finance department know that your preliminary budget is way lower than what you'll ask for later. If you're not sure what a preliminary budget is, read ["Budgets, execution and realization"](/public-sector) in the Basics section.

**2. Have the assembly pass your budget.** Tell the representatives who vote your budget how low and reasonable it is. They'll pass it happily and tell their voters that they keep government spending in check.

**3. Prepare your budgetary amendments.** Now is the time to ask for real money. Prepare a list of items you want. You need to draft an amendment to the initial budget for each. Having a lots of amendments means you'll flood the representatives who'll review them. You can even put a few outrageous amendments in the lot so that representatives waste time on those and pass all the others!

**4. Enjoy your new budget.** You can easily double your initial budget using amendments. The money won't go to your pocket directly, but with your new budget, you can consider preparing a few [Kickbacks](/the-kickbacks) - just follow [the recipe](/the-kickbacks)!

!!! inreallife "In real life"
	In **Israel**, changes in the national budget amount to over 10% of the total budget and much more than that when it comes to military spending. According the a member of the national parliament, Stav Shaffir, ministers "show the public a picture of the budget, while in reality they're doing something completely different. It's simply a joke." Read [Money transfers make for a 'fictitious budget'](http://www.ynetnews.com/articles/0,7340,L-4746044,00.html) for more.

	In **France**, the recipe is used the other way around. The initial budget foresees a large amount for research, which is systematically cut short by 6% (1 billion Euros) with budgetary amendments. Read [Budget de la recherche: l'aveu](http://sciences.blogs.liberation.fr/2015/07/09/budget-de-la-recherche-laveu/) at Libération.


!!! whatolookfor "What to look for"
	Look for the changes made to the preliminary budget over the year (such changes must be voted by the assembly of the territory). If these changes amount to more than a few percents of the initial budget, something's wrong.
