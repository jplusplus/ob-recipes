Title: The Capitalized Rents
Summary: Your city has a hard time balancing a budget? With this little trick no one will suspect this and you will be able to continue investing.
Slug: the-capitalized-rents
Lang: en
Ingredients: 
	An executive position in a city
Serves: n/a
Risk: None
Duration: One shot


**1. Lease buildings that belong to the city to friendly social housing companies** under an emphyteutic lease. Social housing companies will have the full benefit and enjoyment of the buildings in exchange for a rent over the next 99 years.

**2. Ask the social housing companies to pay you in one shot the capitalized amount of rents for the duration of the lease** (usually between 60 and 99 years). Instead of paying you a rent every year, they pay all the rents of all the years in one go. Don't worry for your friends' financing, they can ask a state-run bank for the money.

**3. Distribute subsidies to the social housing companies**, so that they can undertake low-cost renovation work in the buildings your rent them.

**4. Ask your accountant to transfer this cash flow that comes from investing activities to the operating section**. This accounting trick will artificially increase the operating income in your budget.

**5. Enjoy your positive cash flow!** Because it now looks like you have a healthy budget, where operating income pays for operating expenditures, you can invest and build whatever prestige project you need for your reelection!

!!! inreallife "In real life"
	In 2016, the mayor of Paris Anne Hidalgo [capitalised €354.4 million of future rents of public housing.](http://www.lesechos.fr/19/05/2016/lesechos.fr/021941656341_la-cour-des-comptes-denonce-les-artifices-comptables-de-la-mairie-de-paris.htm) Then, using the accounting trick of transferring these investment incomes into operating revenues, allowed her to create a perfectly well-balanced budget despite the fact that real operating income decreased.


!!! whatolookfor "What to look for"
	Transferring the revenues of investing activities to the operating section is not a normal accounting process and it's permitted only in very exceptional and well-defined circumstances. The official request is a public document that you can find in the minutes of council meetings.