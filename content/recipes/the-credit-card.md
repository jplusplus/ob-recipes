Title: The Credit Card
Summary: A classic recipe that everyone loves!
Tags: highlight, appetizer
Slug: the-credit-card
Lang: en
Ingredients: 
	Access to your public body’s means of payment
Serves: up to 100,000€
Risk: High
Duration: Less than 5 years


**1. Get yourself elected or nominated to the position of treasurer** of a small public administration. In most public bodies and large companies, the treasurer can only spend money after someone authorized him or her to do so (usually called "authorizing officers"). This is the "four-eyes principle": Two persons are required for any type of spending. In small structures, there is no room for the four-eye principle - the treasurer is king.

Thus, the position of treasurer gives you access to the institution’s bank account and credit card.

**2. Spend freely!** It’s your name on the credit card, after all!

**3. Hide.** The downside of this recipe is that your spending spree is visible to anyone looking at the banking statement of your institution. Even if you take only cash, amounts above a few thousand euros will be noticed. Hoping that the audit will come after the statute of limitations has kicked in is an option, but it’s a risky one.


!!! inreallife "In real life"
    The treasurer of a fire station in **Kentucky** spent 123,000$ in restaurants, gifts and fireworks using the credit card of the fire station (read in the [Lexington Herald-Leader](http://www.kentucky.com/news/politics-government/article44386299.html).

    In **Sweden**, it emerged in the early 1990's that Mona Sahlin, a member of the government, had used her professional credit card to pay for private rental cars, withdraw cash and buy two bars of Toblerone - in what became known as the Toblerone Affair. In total, she took 50,000 Swedish Kronas (about 5,000€) from her credit card (she paid it back when the affair came to light). No charges were brought because prosecutors could not find a clear case of misdemeanor, though her political career was damaged. Read [the story in Aftonbladet](http://www.aftonbladet.se/nyheter/article10900392.ab) [se].
