Title: The Direct Bank Transfer
Summary: A quick and easy recipe for assemblies that can yield millions!
Lang: en
Slug: the-direct-bank-transfer
Ingredients: 
	A Member of the European Parliament (MEP)
	One bank account of your choosing
Serves: up to €10 million
Risk: Low
Duration: As many mandatures as you can

**1. Get yourself elected as a member of an assembly.** It makes no difference which political party you are affiliated with, but bigger parties can cook bigger portions. 

**2. Give your bank account details** to the finance department of the assembly or to the tresurer of the group you're affiliated with.

**3. Have the Parliament deposit money into the account.** Some assemblies (the European Parliament, for instance) allow for the money to flow directly to your account. Others might require that the bank account be in the name of a non-profit but might not check that it is true.

!!! inreallife "In real life"

At the *European Parliament*, membres of parliament (MEPs) can use a monthly allowance of €4,320 in any way they see fit (on top of the salary), they just need to give the Parliament’s Directorate General of Finance the bank account's details.
Who knows how much is spent on office expenses? No one, as the Parliament has not a single financial control mechanism in place to ensure MEPs are spending this allowance within the rules.

In *France*, the conservative group at the national Senate apid its members end-of-year bonuses as well as direct payments (through intermediaries such as NGOs) amounting to dozens of millions of euros over ten years. [Read more at Mediapart](https://www.mediapart.fr/journal/france/240117/revelations-sur-les-millions-d-euros-siphonnes-par-des-senateurs-ump).