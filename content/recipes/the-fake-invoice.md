Title: The Fake Invoice
Summary: When the public purse pays real money for a fake service.
Slug: fake-invoice
Lang: en
Ingredients: 
	A position of decision-maker in the public works department of your public administration
Serves: Up to €1 million
Risk: High
Duration: 5 years

_This is the equivalent of_ The Invisible Public Works _recipe in the field of services._

**1. Get yourself appointed in a position to pass contracts.** Most department heads can do this.

**2. Reach an agreement with a private company** whereby you award them contracts for services that they do not have to do. You can, for instance, pay them for computer-related services that they do not have to do. Simply ask them for an invoice of the needed amount.

**3. Wait for your share of the invoice.** Your public administration might pay, for instance, €10 million to the contractor. The contractor will thank you with about €1 million in cash and other benefits (a nice car, for instance).

**4. Hedge against the risks!** This recipe is especially risky because, if you get caught, you will be treated as the single bad apple, even if the whole department was corrupt. Former colleagues, even if they benefited from largesses as well, will not share the blame. Prosecutors in Europe rarely investigate more than one person in such corruption cases. You do not want that person to be you.

!!! inreallife "In real life"
    In **Switzerland**, an employee of the state secretariat for the economy (Seco) faced corruption charges for having signed off over CHF 10 million in bogus contracts. She received CHF 700,000 in cash and CHF 200,000 in in-kind gifts for this. Even though evidence that the whole Seco benefited from the deals, no one else was charged.

    Read [Seco-Affäre: Mit fiktiven Rechnungen eine Million ertrogen](http://www.genios.de/presse-archiv/artikel/TAS/20160717/seco-affaere-mit-fiktiven-rechnunge/JM20160717000824997.html) (€) in SonntagsZeitung for the details of the invoices and [Seco-Beamter soll eine Million Franken kassiert haben](http://www.tagesanzeiger.ch/schweiz/standard/SecoBeamter-soll-eine-Million-Franken-kassiert-haben/story/25535955?dossier_id=2550) for the story on the whole department.

    In *France*, the city of Lambersart contracted a company to give fully fictional trainings. The company is very close to the party of the city mayor. [Read more at L'Express](http://www.lexpress.fr/actualite/societe/justice/exclusif-marc-philippe-daubresse-accuse-de-detournement-de-fonds-publics_1872274.html).

!!! whatolookfor "What to look for"
	Fake invoices are extremely hard to detect, because you need to know the details of the invoice to assess if the work has actually been carried out. Most of the time, freedom of information requests on invoices or detailed offers are refused on corporate privacy grounds (disclosing the details of the offer would give competitors an advantage, they claim). Insider tips are your best chance.