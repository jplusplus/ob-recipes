Title: The Invisible Public Works
Summary: A public contract usually involves two things: doing some work and receiving payment. Let your contractor skip the first part and enjoy some of the second one!
Slug: the-invisible-public-work
Lang: en
Ingredients: A position of decision-maker in the public works department of your public administration
             A program of repairs
Serves: up to €10 million
Risk: Low
Duration: 20 years

_This is the equivalent of _The Fake Invoice_ recipe in the field of public works._

**1. Get yourself appointed in the public works department.** You need to be in a position where you can choose which companies will be awarded public works contracts.

**2. Pass a contract for road repairs** (any kind of repairs will do). The construction company that gets the contract must know that it does not have to actually do the work but _you should never put it in writing_! The best time to do such deals is when the construction company invites you to dinner or lunch (they very often do, when you take you position).

Variation: You can pass a contract for a road which does not exist for juicer results (it will increase the risk, so be careful).

**3. Validate the work.** The work not carried out by the construction company need to be validated. If you do not want to ceertify that the work was properly executed, you can delay the validation for years, until no one cares about it anymore.

**4. Wait for your envelope**. The contractor you awarded the contract to will come back to you with a fat envelope of cash next time you meet for lunch! 

![A damaged road.](../recipes/damaged-road.jpg)

<small>A damaged road. Or maybe a road that has just been repaired with <em>The Invisible Public Works</em></small>

!!! inreallife "In real life"
    This recipe is inspired by the numerous cases of corruption in **Frankfurt**, Germany, in the late 1980's, reported in _Korruption in Deutschland: Portrait einer Wachstumsbranche_, an excellent book by Britta Bannenberg and Wolfgang J. Schaupensteiner (pages 108-114).

    In **Berlin**, in the early 2010's, work by Imtech, a Dutch company, worth €65 million for the Berlin airport construction site was accepted and paid for even though it had not been carried out. Imtech is accused of having given millions in kickbacks to people responsible for the construction of the Berlin airport. (Read [the story at Tagesspiegel](http://www.tagesspiegel.de/berlin/berlin-schoenefeld-korruption-am-flughafen-ber-die-akte-imtech/11464662.html), in German).

!!! whatolookfor "What to look for"
	The public contracts for road repairs are public. Once you obtained them, you can check that the work has been carried out by going on location, or using Google Earth, which has a "history" button to see previous satellite pictures of the same location.
