Title: The Overpaid Staff Member
Summary: Delight your friends and family with the budget set by Parliament for staffing arrangements.
Lang: en
Slug: the-overpaid-staff-member
Ingredients: 
	One Member of the European Parliament (MEP)
	One recruitment of a local parliamentary assistant or service provider
Serves: up to €1,403,520
Risk: Medium
Duration: One 5-year European Parliamentary Legislative mandate

**1. Get elected as a Member of the European Parliament.** There are 751 seats up for grabs every 5 years. For this recipe, it makes no difference which political party you are affiliated with.

**2. Hire a staff member or service provider in your local constituency.** Each MEP is provided €23,392 per month of public money to spend on staffing arrangements.

**3. Fiddle invoices and produce dodgy documents while enjoying a nice class of Chateau neuf du taxpayer.** With the right amount of skill and subtlety fiddling the staff allowances is possible. But getting caught has its downsides.      

!!! inreallife "In real life"
**Peter Skinner**, ex-MEP, was recently sentenced to 4 years in jail for fraudulently [using approximately  £100,000 of his staff allowance](http://www.politico.eu/article/peter-skinner-mep-jailed-expenses-fraud-guilty-theft-corruption-stealing-labour/) to pay for lavish vacations, jewellery, and even thousands of euros in alimony payments to his ex-wife. He was able to do this by submitting documents and inflated invoices on local assistant and service provider costs. The excess funds 