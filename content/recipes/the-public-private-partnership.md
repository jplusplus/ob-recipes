Title: The Public-Private Partnership
Summary: The perfect recipe to increase your popularity, the popularity of your party and make new friends in high places.
Lang: en
Slug: the-public-private-partnership
Ingredients: A position of decision-maker in a local public sector authority.
Serves: Your popularity
Risk: None
Duration: 10 years

**1. Find out what new building your voters would like**. It can be a new stadium, a new hospital, a school, a bridge or anything that will make you popular.

**2. Establish a public-private partnership with a private construction company**. This partnership means that the private company will pay the bulk of the construction costs and share the risks of operating the equipment. At least, in theory. Because you don't want to let your friends shoulder all the risks related to the construction and the operation, you will let them keep all revenues from the equipment. In case that's not enough, you'll provide them with a minimum guaranteed income from the city, a rent.

Note: You can choose any construction company but it has to be big and powerful enough to have enough investment capacity. Don't forget to read our recipe on [Kickbacks](http://www.cookingbudgets.com/the-kickbacks/) before your make a call for tenders!

**3. Congratulations, your city has now a new building that cost almost nothing**, at least for now. Your administration has now to pay rent to the private company to use it. The total cost of the rent over 30 years will be higher than the cost of building the equipment but don't worry, you will already be retired when people will realize this. 

**4. Promote widely your new project to the citizens** with strong emphasis on all the savings made by the city thanks to the private construction company. You can thank them for their generosity and for constructing a new, modern, cutting-edge technology building in the city.

**5. Your new popularity will not be to everyone's liking and might also provoke jealousy.** Your political opponents or some citizens associations can dig into documents and argue that all the expenses involved by this public-private partnership are higher than a normal public procurement. Don't hesitate to mock the alternative facts of these kill-joys.

!!! inreallife "In real life"
	With Euro2016 coming, the city of Bordeaux, France asked private companies to build a stadium. Officially, the city paid only 19 million euros. But the devil is in the details! When we add up the commitments the city took for the next 30 years, the cost for Bordeaux's taxpayers is €220 million, or €201 million more than planned! Faced with such contradictions, the local strongman, Alain Juppé, had a bullet-proof answer. The data is "fanciful", he said.
	[Source and more data here](http://www.thefootballtax.com/#!/stadiums/fra-bordeaux-matmut-atlantique).


!!! whatolookfor "What to look for"
	Whether a building is constructed under a public-private partnership or not is public information. Despite this, the whole contract of the partnership is almost impossible to obtain through a FOIA. The administration will probably refuse to communicate it to protect commercial and industrial confidentiality.

