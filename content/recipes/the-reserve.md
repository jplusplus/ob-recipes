Title: The « Réserve Parlementaire »
Summary: Just like « montée au beurre », some terms of the French cuisine are not translatable.
Lang: en
Slug: 
Ingredients: 
	A position as a member of the French parliament
Serves: €150,000 / year
Risk: None
Duration: 5 years

This recipe works only in the French parliament, but the « Réserve » tastes so good we decided to publish it anyway.

**1. Get elected in the French parliament.**
There are about 400 seats as members of the Senate and 600 as members of the Chamber of Representatives to be had every five years (every six years for the Senate). The seat that requires the least votes to enter parliament is in the territory of Saint-Pierre-and-Miquelon, in the North Atlantic. There, you need just 1,700 votes to get your seat - and your « Réserve »!

**2. Make a list of beneficiaries of your « Réserve »**. Every year, you'll recieve 150,000€ to spend on organizations, NGOs or city councils. Just make a list and choose an amount for each. Send the list to the ministry of Interior, which does the actual payment, and you're all set.

You can choose to give all the money to your home town (remind people that they should vote for you!), to the horse club of your wife or to the village where you spend your holidays.

**3. That's it!** There's no risk at all! After all, you're the lawmaker, aren't you?

!!! inreallife "In real life"

Every year, €150m flow from French taxpayers to the pet projects of members of parliaments, with no oversight at all. Read [La réserve parlementaire, source de subventions et de conflits d'intérêts](http://www.liberation.fr/france/2016/09/27/la-reserve-parlementaire-source-de-subventions-et-de-conflits-d-interets_1469763) [fr].

!!! whatolookfor "What to look for"

Since 2013, the details of the « Réserve » are public. Before that date, the archive is not available.