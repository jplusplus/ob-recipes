Title: The Revolving Door
Summary: Cook in the public sector while your next position awaits in a large corporation!
Tags: highlight
Slug: the-revolving-door
Lang: en
Ingredients: 
	A very high-ranking position in a civil administration
Serves: up to €1 million
Risk: None
Duration: Unlimited

**1. Access a position of power within a public administration**. Minister, Chancellor or Commissioner will give you the best results.

**2. Take decisions beneficial to the company that will hire you.** Of course, when you take these decisions, you argue that they are absolutely needed for the good of the country, region or city.

**3. Take your next job.** The company whose interest you served while holding a public office can now hire you with a yummy salary. You might have to testify in front of an ethics committee that you were not bribed while in office. Just say you weren't.

!!! inreallife "In real life"
	**Gerhard Schröder**, German Chancellor, pushed hard for the construction of the Russia-Germany Nord Stream pipeline (a €7 billion project) while in office, thereby making Germany dependent on Russian gaz and angering the Baltic states and Poland. Upon leaving office in 2005, he became chairman of the shareholders' committee of Nord Stream. Read more at [The Guardian](http://www.theguardian.com/world/2005/dec/13/russia.germany).

	**Neelie Kroes**, former European Commission commissioner, joined the board of taxi service Uber in May 2016, after waiting the legal window of 18 months after she left the Commission. While in office, she defended Uber very strongly, going as far as insulting ministers of Member States who criticized the company. Read more at [TechCrunch](http://techcrunch.com/2016/05/06/uber-appoints-former-ec-vp-neelie-kroes-to-its-public-policy-board/) and [one of her rants](http://ec.europa.eu/archives/commission_2010-2014/kroes/en/content/crazy-court-decision-ban-uber-brussels-show-your-anger.html) as a Commissioner. This was not the first time she used the Revolving Door. In 1990, she joined the board of Ballast-Nedam, a public works company involved in a corruption scandal that took place during her stint as transport minister (read the story in German [here](https://www.uni-muenster.de/NiederlandeNet/nl-wissen/wirtschaft/vertiefung/betuweroute/korruption.html)).

!!! whatolookfor "What to look for"
	After a politician leaves office, check where he or she takes work at and whether there could be a conflict of interest. For second-tier politicians, just follow their LinkedIn or XING profiles to find out.
