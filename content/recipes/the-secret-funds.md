Title: The Secret Funds
Summary: If you know where to look, you'll find cash reserves in your public administration just waiting to be plundered!
Tags: highlight
Slug: the-secret-funds
Lang: en
Ingredients: 
	A position along the chain of command of secret funds (minister of Interior, head of police...)
Serves: up to €1 million
Risk: Medium
Duration: Less than 5 years


**1. Get access to sluch funds.** Most ministries of Interior have cash reserves for special operations, such as paying informants in the police. These funds are part of the budget of the public body and are given in cash. You must have access to this cash at some point.

**2. Take some cash in your pocket from time to time.** With €200 bills, €10,000 fits in a standard enveloppe.

**3. Deny.** If someone ask why cash reserves have diminished, just say the budget was cut. After all, informants probably are bad guys and don't need that much money!

**4. Make sure that you take care of potential witnesses!** Secret funds leave no written trace, but humans can complain if you're careless.

!!! inreallife "In real life"
	In France, the former chief of staff of the Interior minister, Claude Guéant, was found guilty of taking 10,000€ monthly during 2 years from a cash reserve for informants (read [in Le Monde](http://www.lemonde.fr/politique/article/2015/11/13/affaire-des-primes-en-liquide-claude-gueant-condamne-a-deux-ans-de-prison-avec-sursis_4809160_823448.html))


!!! whatolookfor "What to look for"
	Because secret funds leave not trace, it's impossible to do freedom of information requests to obtain information. The only people who might help you are those down the line (police detectives) who know if their secret funds budget decreased suspiciously.
