**Have a position within a public administration? Need cash? Follow any of these yummy recipes and enjoy!**

This website is obviously [satire](https://en.wikipedia.org/wiki/Satire). This is actually a collection of tutorials that explain how journalists and civil society activists can investigate public budgets to find story leads.

Want to contribute? Send us a message at chef@cookingbudgets.com
