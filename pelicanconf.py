#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import markdown
import codecs

# Edit this ############################################################

AUTHOR = u'Journalism++'
SITENAME = u'40 Recipes for Cooking Public Budgets'
SITEURL = ''
GOOGLE_ANALYTICS = 'UA-35984661-9'

META_DESCRIPTION = "Have a position within the administration? Need cash? This series of recipes will help you."
META_SHARE_IMAGE = "https://jplusplus.gitlab.io/ob-recipes/theme/images/cover-mobile.jpg"

BACK_TO_RECIPES_STRING = "Back to Recipes"
BACK_TO_BASICS_STRING = "Back to Basics"

# Set up i18n -- add new languages here
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["i18n_subsites"]
#I18N_SUBSITES = {
#     'fr': {
#         'SITENAME': '40 Recettes',
#         'LANGNAME': 'Français',
#         'BACK_TO_RECIPES_STRING': "Retourner aux recettes",
#         'BACK_TO_BASICS_STRING': "Retourner aux basiques",
#         'WELCOME_TEXT': markdown.markdown(codecs.open("content/welcome_text-fr.md", 'r', 'utf-8').read(), output_format="html5", encoding="UTF-8")
#     }
# }

# All good! Below is intricate techy stuff. ##########################

PATH = 'content'

WELCOME_TEXT = markdown.markdown(codecs.open("content/welcome_text.md", 'r', 'utf-8').read(), output_format="html5", encoding="UTF-8")

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'en'

# No feeds for development
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# No pagination
DEFAULT_PAGINATION = False

# Use the filesystem date instead of Date field
DEFAULT_DATE = 'fs'
# Folder = category
USE_FOLDER_AS_CATEGORY = True

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = 'themes/recipes'
CSS_FILE = 'app.css'

ARTICLE_URL = '{slug}/'
ARTICLE_SAVE_AS = '{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
DRAFT_URL = 'private/{slug}/'
DRAFT_SAVE_AS = 'private/{slug}/index.html'
DRAFT_LANG_URL = 'private/{slug}-{lang}/'
DRAFT_LANG_SAVE_AS = 'private/{slug}-{lang}/index.html'

STATIC_SAVE_AS = '{path}'
STATIC_URL = '{path}'
# Make sure images in content paths are copied to output
STATIC_PATHS = ['basics', 'recipes', 'recettes']
ARTICLE_PATHS = ['basics', 'recipes', 'recettes']

MD_EXTENSIONS = ["markdown.extensions.footnotes", "markdown.extensions.admonition"]
HIGHLIGHT_TAG = "highlight"
