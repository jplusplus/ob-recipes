$(document).foundation()

/* jQuery to center the image from
https://stackoverflow.com/questions/3300660/how-do-i-center-an-image-if-its-wider-than-its-container
*/

$("#image-banner > div.columns > img").each(function(i, img) {
    $(img).css({
        position: "relative",
        left: ($(img).parent().width() - $(img).width()) / 2
    });
});
